Readme File for my Python Airplane Scheduling Project 2
Last Updated: Sunday, 2/23/2020 
Author: Shamarr McKinney-VanBuren 
Latest version available: bitbucket.org/shamarrmckinney/mckinneyvanburenpythonproject2/src 

== Overview ==

This program uses Python to simulate aircraft take off time slots at an airport.

It records and maintains the information needed to schedule the airstrip resource: request identifier, request submission time, time slot requested, length of time requested, actual start time, actual end time. 

To run program successfully, must have an input text file with the above quantitative information specified. Each flight must have its own line of information.

Test input file is included in above bit bucket repository

Source code found in src folder in above bit bucket repository. 

Said input text file NEEDS to be in a directory that the program can find. Recommended to put the text file and the program in the same directory path.  


=======================================================================