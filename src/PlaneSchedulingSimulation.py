#This Program is an Airplane Scheduling Simulation
#
#Shamarr McKinney-VanBuren

from queue import PriorityQueue

#This function reads into file and pulls data, creating list of tuples
#Returns a list of tuples of file data
def addToWaitingList( chosenFile) :

    with open(str( chosenFile)) as chosenFile:
        for line in chosenFile: #Removing title header
            if "ID, Submission Time, Requested Start, Requested Duration" :
                break
        listOne = [tuple(line.strip().replace(' ', '').split(',')) for line in chosenFile] 

    return listOne #return list of tuples
        
    chosenFile.close()



#This function takes in a list and creates a priority queue, prioritizing by tuple elements (submission time and requested start time)
#Returns a priority queue of plane file data, prioritized by submission time and requested start
def sendToQueueBasedOnPriority( listOne) :

    queueTwo = PriorityQueue()

    for x in listOne:
        queueTwo.put((x[1],x[2], x[3], x[0])) # Looking through list and putting into priority queue based on submission time, requested start time & duration time

    tempList = list(queueTwo.queue)    
        
    return tempList



#A function to schedule and keep track of time in this simulation
#Returns a complete scheduled list of planes

def simulate( listTwo) : #LOGIC IS NOT YET CORRECT!!!
    time = 0
    listThree = []

    while time < 24: #24th hour being the final hour of the day
        if len(listTwo) > 0 : #As long as listTwo is not empty
            for x in listTwo : #Loop through elements in listTwo
                if int( x[1]) == time : #if requested start == current time
                    if int( x[1]) == [int( y[1]) for y in listThree] : #if requested start is equal to a requested start of a plane already in listThree
                        listThree.insert(int (x-1[2] + 1), list(x[0:4])) #Insert after the previous elements duration length
                        #listThree.append(x[int (x-1[2] + 1)])
                    else:   #if requested start is not equal to a requested start of a plane already in listThree
                        listThree.insert(time, list(x[0:4]))
                else:  # Requested start time is not equal to current time
                    listThree.insert(int( x[1]), list(x[0:4]))
                time +=1

        else:
            print('List Two is Empty')
        



    return listThree #Return properly scheduled plane list



#A function to nicely print out contents of our final, scheduled list
#Returns a nicely printed list
def prettyprint( printList) :

    for x in printList :
        print( x[3], 'lands at time', x[1], 'with a landing duration of', x[2])


    return printList
        

      
                
