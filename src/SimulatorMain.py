#This module acts as the 'Driver' to the Plane Scheduling Simulation module
#
#Shamarr McKinney-VanBuren

import PlaneSchedulingSimulation
import sys

def main() :

    firstList = PlaneSchedulingSimulation.addToWaitingList(str(sys.argv[1])) #test.txt
    print(firstList) #Printing initial list of data in tuples
    secondList = PlaneSchedulingSimulation.sendToQueueBasedOnPriority( firstList)
    print( secondList) #Returns Sorted list based on submission time and then requested start time
    scheduleIt = PlaneSchedulingSimulation.simulate( secondList)
    print( scheduleIt)  #Returns Sorted list based on plane scheduling time
    printIt = PlaneSchedulingSimulation.prettyprint( scheduleIt)
    print( printIt) #Prints put final schedule list nicely


if __name__ == "__main__" :
    argnum = len(sys.argv)
    main()
    print( "There were", argnum, "arguments on the command line")
